let $VIM_BUILD_FOLDER='.'
let $VIM_SRC_FOLDER='..'
let $VIM_BIN_NAME='QtWalkthrough'
let $VIM_COMPILE_COMMAND="true"
let $VIM_BUILD_COMMAND='bear -- make'
let $VIM_INSTALL_COMMAND='make install'
let $COMPILE_COMMANDS_JSON='compile_commands.json'

function! TagIt()
  silent !rm -frv tags 
  silent !echo "Building tags..."

  silent !find . -name '*.c' -o -name "*.cc" -o -name '*.cpp' -o -name '*.h' -o -name '*.hpp' > project.files
  silent !cat project.files | ctags --verbose=yes --sort=yes --c++-kinds=+p --fields=+iaS --extras=+q --language-force=C++ -f tags -L -

  silent !find /usr/include/X11/ -name '*.c' -o -name "*.cc" -o -name '*.cpp' -o -name '*.h' -o -name '*.hpp' > x11.files
  silent !cat x11.files | ctags --verbose=yes --sort=yes --c++-kinds=+p --fields=+iaS --extras=+q --language-force=C++ -f x11tags -L -
  set tags+=./x11tags

  silent !echo "Done"
  redraw!
endfunction

function! Clean()
  silent !clear
  !rm -frv $VIM_BUILD_FOLDER/**
  redraw!
endfunction

function! Build()
  !mkdir -p $VIM_BUILD_FOLDER && cd $VIM_BUILD_FOLDER && $VIM_COMPILE_COMMAND $VIM_SRC_FOLDER && $VIM_BUILD_COMMAND -j$CORES && $VIM_INSTALL_COMMAND
  redraw!
endfunction

function! LinkCompileCommands()
  silent !mkdir -p $VIM_BUILD_FOLDER && cd $VIM_BUILD_FOLDER && $VIM_COMPILE_COMMAND $VIM_SRC_FOLDER && $VIM_BUILD_COMMAND -j$CORES && ln -sf $(find ./${VIM_BUILD_FOLDER}/ -name "$COMPILE_COMMANDS_JSON") ./$COMPILE_COMMANDS_JSON
  redraw!
endfunction

function! Debug()
  !cd $VIM_BUILD_FOLDER && if [ -f "$VIM_BIN_NAME" ]; then gdb ./$VIM_BIN_NAME; fi
  redraw!
endfunction

nmap <F4> :call TagIt()<CR>:call LinkCompileCommands()<CR>
nmap <Leader><F4> :call Build()<CR>
nmap <Leader><F5> :call Run()<CR>
nmap <Leader><F6> :call Debug()<CR>

function! Switch()
  let filename = expand("%:t:r")
  let fileext = expand("%:e")
  if (fileext == "c")
    find %:t:r.h
  endif
  if (fileext == "h")
    find %:t:r.c
  endif
endfunction

set shell=/bin/bash
