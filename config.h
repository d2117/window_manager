/* See LICENSE file for copyright and license details. */

#include <X11/XF86keysym.h>
#include <X11/keysymdef.h>
/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Roboto Mono:pixelsize=15:antialias=true:autohint=true" };
static const char dmenufont[]       = "Roboto Mono:pixelsize=15:antialias=true:autohint=true";
static const char dmenu_history[]   = "/bin/.dwm/dmenu_history";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char col_cyan_light[]  = "#00ffbf";
static const char col_dark_green[]  = "#003300";
static const char *colors[][4]      = {
	/*                     fg         bg               border   */
	[SchemeNorm]      = { col_gray3, col_gray1,       col_gray2 },
	[SchemeSel]       = { col_gray4, col_cyan,        col_cyan_light },
	[SchemeHid]       = { col_cyan,  col_gray1,       col_gray2},
	[SchemeNoActive]  = { col_gray4, col_dark_green,  col_gray2}
};

/* tagging                     0    1   2   3   4   5   6*/
static const char *tags[] = { "爵","","","","","","וֹ"};
static const int tags_keys[] = { XK_1, XK_2, XK_3, XK_4, XK_5, XK_6, XK_0};
enum TAGS_NAMES {
  WEB_TAG,
  TERMINAL_TAG,
  MUSIC_TAG,
  DOCUMENT_TAG,
  PHOTO_TAG,
  SETTINGS_TAG,
  SOCIAL_TAG
};

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class          instance        title    tags mask     isfloating   monitor */
	{ "alacritty",     NULL,          NULL,    1 << TERMINAL_TAG,  0,           -1 },
	{ "apvlv",				 NULL,          NULL,    1 << DOCUMENT_TAG,  0,           -1 },
	{ "Gimp",          NULL,          NULL,    1 << PHOTO_TAG,     1,           -1 },
	{ "firefox",       NULL,          NULL,    1 << WEB_TAG,       0,           -1 },
	{ "Firefox",       NULL,          NULL,    1 << WEB_TAG,       0,           -1 },
	{ "google-chrome", NULL,          NULL,    1 << WEB_TAG,       0,           -1 },
	{ "Google-chrome", NULL,          NULL,    1 << WEB_TAG,       0,           -1 },
	{ "chromium",      NULL,          NULL,    1 << WEB_TAG,       0,           -1 },
	{ "Chromium",      NULL,          NULL,    1 << WEB_TAG,       0,           -1 },
	{ "pavucontrol",   NULL,          NULL,    1 << SETTINGS_TAG,  0,           -1 },
	{ "Pavucontrol",   NULL,          NULL,    1 << SETTINGS_TAG,  0,           -1 },
	{ "slack",         NULL,          NULL,    1 << SOCIAL_TAG,    0,           -1 },
	{ "Slack",         NULL,          NULL,    1 << SOCIAL_TAG,    0,           -1 },
	{ "skype",         NULL,          NULL,    1 << SOCIAL_TAG,    0,           -1 },
	{ "Skype",         NULL,          NULL,    1 << SOCIAL_TAG,    0,           -1 },
	{ "Viber",         NULL,          NULL,    1 << SOCIAL_TAG,    0,           -1 },
	{ "ViberPC",       NULL,          NULL,    1 << SOCIAL_TAG,    0,           -1 },
	{ "Telegram",      NULL,          NULL,    1 << SOCIAL_TAG,    0,           -1 },
	{ "zoom",          NULL,          NULL,    1 << SOCIAL_TAG,    0,           -1 },
	{ "GuitarPro",     NULL,          NULL,    1 << MUSIC_TAG,     0,           -1 },
	{ "alacritty",     NULL,          "cmus",  1 << MUSIC_TAG,     0,           -1 },
	{ "libreoffice",   NULL,          NULL,    1 << DOCUMENT_TAG,  0,           -1 },
	{ "okular",        NULL,          NULL,    1 << DOCUMENT_TAG,  0,           -1 }
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

#include "layouts.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[M]",      monocle }, /* first entry is default */
	{ "[]=",      tile },    
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "HHH",      grid },
	{ "CCC",      tcl },
};

enum LAYOUTS_NAMES {
  MONOCLE,
  TILE,
  FLOATING,
  GRID,
  TCL
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggletag,      {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
  { ShiftMask|ControlMask,        KEY,      toggleview,     {.ui = 1 << TAG} }, 
  
/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/*
 * USE ONLY ABSOLUTE PATHS!!!!!!!
 */

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, "-H", dmenu_history, NULL };

// Screen settings
static const char *lock_screen[]                      = { "/usr/bin/slock", NULL };
static const char *brightness_down[]                  = { "/bin/.dwm/brightness.sh", "-", "0.1", NULL };
static const char *brightness_up[]                    = { "/bin/.dwm/brightness.sh", "+", "0.1", NULL };

// Screenshot
static const char *take_screenshot[]                  = { "/bin/.dwm/take_screenshot.sh", NULL };
static const char *take_screenshot_partially[]        = { "/bin/.dwm/take_screenshot.sh", "-p", NULL };

// Terminal
// static const char *termcmd[]                          = { "alacritty", NULL };
static const char *termcmd[]                          = { "/bin/.dwm/launch_terminal.sh", NULL };

// Musical player
static const char *cmus[]                             = { "st", "-t", "cmus", "-e", "cmus", NULL };
static const char *cmus_toggle_pause[]                = { "cmus-remote", "-u", NULL };
static const char *cmus_stop[]                        = { "cmus-remote", "-s", NULL };
static const char *cmus_next[]                        = { "cmus-remote", "-n", NULL };
static const char *cmus_prev[]                        = { "cmus-remote", "-r", NULL };
static const char *cmus_vol_up[]                      = { "cmus-remote", "--volume", "+5%", NULL };
static const char *cmus_vol_down[]                    = { "cmus-remote", "--volume", "-5%", NULL };

// Sound sets
static const char *downvol[]                          = { "/bin/pamixer", "-d", "5", NULL };
static const char *upvol[]                            = { "/bin/pamixer", "-i", "5", NULL };
static const char *highvol[]                          = { "/bin/pamixer", "--set-volume", "100", NULL };
static const char *lowvol[]                           = { "/bin/pamixer", "--set-volume", "20", NULL };
static const char *mute_toggle_vol[]                  = { "/bin/pamixer", "-t",  NULL };
// Microphone sound
static const char *mic_downvol[]                      = { "/bin/.dwm/mic_vol_control.sh", "-d", "5", NULL };
static const char *mic_mute_toggle_vol[]              = { "/bin/.dwm/mic_vol_control.sh", "-t",  NULL };
static const char *mic_upvol[]                        = { "/bin/.dwm/mic_vol_control.sh", "-i", "5", NULL };
static const char *switch_vol_source[]                = { "/usr/bin/pavucontrol", "-t", "5", NULL };

// Poweroff and reboot
static const char *poweroff[]                         = { "poweroff", NULL };
static const char *reboot[]                           = { "reboot", NULL };

// Activate deactivate update status
static const char *toggle_status_bar[]                = { "/bin/.dwm/status_bar.sh", "-t", NULL };

// Change active input language
static const char *change_language[]                  = { "/bin/.dwm/change_keyboard_layout.sh", NULL };


#include "movestack.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_r,                        spawn,          {.v = dmenucmd } },
  { MODKEY,                       XK_grave,                    spawn,          {.v = termcmd } },
  { MODKEY,                       XK_grave,                    view,           {.ui = 1 << TERMINAL_TAG } },
	{ MODKEY,                       XK_b,                        togglebar,      {0} },
  // Moving around the windows
	{ MODKEY,                       XK_j,                        focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,                        focusstack,     {.i = -1 } },
  // Moving around the tags
	{ MODKEY,                       XK_equal,										 view_adjacent,  {.i = +1 } },
	{ MODKEY,                       XK_minus,										 view_adjacent,  {.i = -1 } },
	{ MODKEY,                       XK_0,                        view,           {.ui = 1 << SOCIAL_TAG } },
	{ MODKEY,                       XK_F5,                       view,           {.ui = 1 << SETTINGS_TAG } },
  // Increase master windows
	{ MODKEY,                       XK_i,                        incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,                        incnmaster,     {.i = -1 } },
  // Resize window
	{ MODKEY,                       XK_h,                        setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,                        setmfact,       {.f = +0.05} },
  // Select window from stack
	{ MODKEY,                       XK_Return,                   zoom,           {0} },

	{ MODKEY,                       XK_Tab,                      view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,                        killclient,     {0} },
  // Change layout
	{ MODKEY,                       XK_c,                        setlayout,      {.v = &layouts[TCL]} },
	{ MODKEY,                       XK_f,                        setlayout,      {.v = &layouts[FLOATING]} },
	{ MODKEY,                       XK_g,                        setlayout,      {.v = &layouts[GRID]} },
	{ MODKEY,                       XK_m,                        setlayout,      {.v = &layouts[MONOCLE]} },
	{ MODKEY,                       XK_t,                        setlayout,      {.v = &layouts[TILE]} },
  // Move window through stack
	{ MODKEY|ControlMask,           XK_Right,                    movestack,      {.i = +1 } },
	{ MODKEY|ControlMask,           XK_Left,                     movestack,      {.i = -1 } },
	{ MODKEY,                       XK_comma,                    focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period,                   focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,                    tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period,                   tagmon,         {.i = +1 } },
  
  // Volume control
  { ShiftMask,                    XK_F2,                       spawn,          {.v = downvol } },
	{ ShiftMask,                    XK_F3,                       spawn,          {.v = upvol   } },
	{ ControlMask|MODKEY|ShiftMask, XK_F3,                       spawn,          {.v = highvol   } },
	{ ControlMask|MODKEY|ShiftMask, XK_F2,                       spawn,          {.v = lowvol   } },
	{ MODKEY|ShiftMask,             XK_F3,                       spawn,          {.v = cmus_vol_up  } },
	{ MODKEY|ShiftMask,             XK_F2,                       spawn,          {.v = cmus_vol_down  } },
	{ MODKEY|ShiftMask,             XF86XK_AudioRaiseVolume,     spawn,          {.v = cmus_vol_up  } },
	{ MODKEY|ShiftMask,             XF86XK_AudioLowerVolume,     spawn,          {.v = cmus_vol_down  } },
  { ShiftMask,                    XK_F1,                       spawn,          {.v = mute_toggle_vol } },
  { MODKEY,                       XK_F2,                       spawn,          {.v = mic_downvol } },
	{ MODKEY,                       XK_F3,                       spawn,          {.v = mic_upvol } },
  { MODKEY,                       XK_F1,                       spawn,          {.v = mic_mute_toggle_vol } },
  // Fn keys volume control
  { 0,                            XF86XK_AudioLowerVolume,     spawn,          {.v = downvol } },
	{ 0,                            XF86XK_AudioRaiseVolume,     spawn,          {.v = upvol   } },
	{ ShiftMask,                    XF86XK_AudioRaiseVolume,     spawn,          {.v = highvol   } },
	{ ShiftMask,                    XF86XK_AudioLowerVolume,     spawn,          {.v = lowvol   } },
  { 0,                            XF86XK_AudioMute,            spawn,          {.v = mute_toggle_vol } },
  { MODKEY,                       XF86XK_AudioLowerVolume,     spawn,          {.v = mic_downvol } },
	{ MODKEY,                       XF86XK_AudioRaiseVolume,     spawn,          {.v = mic_upvol } },
  { MODKEY,                       XF86XK_AudioMute,            spawn,          {.v = mic_mute_toggle_vol } },
  
  // Print screen
  { 0,                            XK_Print,                    spawn,          {.v = take_screenshot } },
  { MODKEY,                       XK_Print,                    spawn,          {.v = take_screenshot_partially } },

  // Lock screen
  { MODKEY|ControlMask,           XK_l,                        spawn,          {.v = change_language } },
  { MODKEY|ControlMask|ShiftMask, XK_l,                        spawn,          {.v = lock_screen } },

  // Control brightness
  { 0,                            XF86XK_MonBrightnessUp,      spawn,          {.v = brightness_up } },
  { 0,                            XF86XK_MonBrightnessDown,    spawn,          {.v = brightness_down } },
  { MODKEY|ControlMask,           XK_Up,                       spawn,          {.v = brightness_up } },
  { MODKEY|ControlMask,           XK_Down,                     spawn,          {.v = brightness_down } },
  
  // Poweroff and reboot
  { MODKEY|ControlMask|ShiftMask, XK_p,                        spawn,          {.v = poweroff } },
  { MODKEY|ControlMask|ShiftMask, XK_r,                        spawn,          {.v = reboot } },

  // Music player cmus
  { MODKEY|ControlMask|ShiftMask, XK_m,                        view,           {.ui = 1 << MUSIC_TAG } },
  { MODKEY|ControlMask|ShiftMask, XK_m,                        spawn,          {.v = cmus } },

  // Cmus playback control
  { 0, 	                          XF86XK_AudioPlay,            spawn,          {.v = cmus_toggle_pause } },
  { 0, 	                          XF86XK_AudioStop,            spawn,          {.v = cmus_stop } },
  { 0, 	                          XF86XK_AudioNext,            spawn,          {.v = cmus_next } },
  { 0, 	                          XF86XK_AudioPrev,            spawn,          {.v = cmus_prev } },
  
  // Pavucontrol
  { MODKEY|ControlMask|ShiftMask, XK_s,                        view,           {.ui = 1 << SETTINGS_TAG } },
  { MODKEY|ControlMask|ShiftMask, XK_s,                        spawn,          {.v = switch_vol_source } },

  { MODKEY|ControlMask|ShiftMask, XK_b,                        spawn,          {.v = toggle_status_bar } },

  // Navigation between tags
	TAGKEYS(                        tags_keys[MUSIC_TAG   ],                     MUSIC_TAG   )
	TAGKEYS(                        tags_keys[PHOTO_TAG   ],                     PHOTO_TAG   )
	TAGKEYS(                        tags_keys[SETTINGS_TAG],                     SETTINGS_TAG)
	TAGKEYS(                        tags_keys[SOCIAL_TAG  ],                     SOCIAL_TAG  )
	TAGKEYS(                        tags_keys[TERMINAL_TAG],                     TERMINAL_TAG)
	TAGKEYS(                        tags_keys[WEB_TAG     ],                     WEB_TAG     )
	TAGKEYS(                        tags_keys[DOCUMENT_TAG],                     DOCUMENT_TAG)

	{ ControlMask|MODKEY|ShiftMask, XK_q,                        quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask                     button         function        argument */
	{ ClkLtSymbol,          0,                             Button1,       setlayout,      {.v = &layouts[MONOCLE]} },
	{ ClkLtSymbol,          0,                             Button3,       setlayout,      {.v = &layouts[GRID]} },
	{ ClkStatusText,        0,                             Button2,       spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,                        Button1,       movemouse,      {0} },
	{ ClkClientWin,         MODKEY,                        Button2,       togglefloating, {0} },
	{ ClkClientWin,         MODKEY,                        Button3,       resizemouse,    {0} },
	{ ClkTagBar,            0,                             Button1,       view_adjacent,  {.i = -1 } },
	{ ClkTagBar,            0,                             Button3,       view_adjacent,  {.i = +1 } },
	{ ClkTagBar,            0,                             Button4,       view_adjacent,  {.i = -1 } },
	{ ClkTagBar,            0,                             Button5,       view_adjacent,  {.i = +1 } },
	{ ClkTagBar,            MODKEY,                        Button1,       tag,            {0} },
	{ ClkTagBar,            MODKEY,                        Button3,       toggletag,      {0} },
  // Switch tags with mousewheel
  { ClkClientWin,         ControlMask|MODKEY,            Button4,       view_adjacent,  {.i = -1 } },
  { ClkClientWin,         ControlMask|MODKEY,            Button5,       view_adjacent,  {.i = +1 } },
  { ClkRootWin,           ControlMask|MODKEY,            Button4,       view_adjacent,  {.i = -1 } },
  { ClkRootWin,           ControlMask|MODKEY,            Button5,       view_adjacent,  {.i = +1 } },
  { ClkStatusText,        ControlMask|MODKEY,            Button4,       view_adjacent,  {.i = -1 } },
  { ClkStatusText,        ControlMask|MODKEY,            Button5,       view_adjacent,  {.i = +1 } },
  { ClkWinTitle,          ControlMask|MODKEY,            Button4,       view_adjacent,  {.i = -1 } },
  { ClkWinTitle,          ControlMask|MODKEY,            Button5,       view_adjacent,  {.i = +1 } },
  // Switch around the windows
	{ ClkClientWin,         MODKEY,                        Button4,       focusstack,     {.i = -1 } },
	{ ClkClientWin,         MODKEY,                        Button5,       focusstack,     {.i = +1 } },
  { ClkRootWin,           MODKEY,                        Button4,       focusstack,     {.i = -1 } },
  { ClkRootWin,           MODKEY,                        Button5,       focusstack,     {.i = +1 } },
  { ClkStatusText,        0,                             Button4,       spawn,          {.v = upvol   } },
  { ClkStatusText,        0,                             Button5,       spawn,          {.v = downvol } },
  { ClkWinTitle,          MODKEY,                        Button4,       focusstack,     {.i = -1 } },
  { ClkWinTitle,          MODKEY,                        Button5,       focusstack,     {.i = +1 } },
  // Adjust volume
	{ ClkClientWin,        ControlMask|MODKEY|ShiftMask,   Button4,       spawn,          {.v = upvol   } },
	{ ClkClientWin,        ControlMask|MODKEY|ShiftMask,   Button5,       spawn,          {.v = downvol } },
  { ClkRootWin,          ControlMask|MODKEY|ShiftMask,   Button4,       spawn,          {.v = upvol   } },
  { ClkRootWin,          ControlMask|MODKEY|ShiftMask,   Button5,       spawn,          {.v = downvol } },
  { ClkStatusText,       ControlMask|MODKEY|ShiftMask,   Button4,       spawn,          {.v = upvol   } },
  { ClkStatusText,       ControlMask|MODKEY|ShiftMask,   Button5,       spawn,          {.v = downvol } },
  { ClkWinTitle,         ControlMask|MODKEY|ShiftMask,   Button4,       spawn,          {.v = upvol   } },
  { ClkWinTitle,         ControlMask|MODKEY|ShiftMask,   Button5,       spawn,          {.v = downvol } },
  // invoke client windows 
  { ClkWinTitle,         0,                              Button1,       focusstack,     {.i = -1 } },
  { ClkWinTitle,         0,                              Button3,       focusstack,     {.i = +1 } },
  { ClkWinTitle,         0,                              Button4,       focusstack,     {.i = -1 } },
  { ClkWinTitle,         0,                              Button5,       focusstack,     {.i = +1 } },
  // Activate/Deactivate status_bar
  { ClkStatusText,       0,                              Button1,       view,           {.ui = 1 << TERMINAL_TAG } },
  { ClkStatusText,       0,                              Button3,       spawn,          {.v = toggle_status_bar } },
};
